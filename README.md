# SparkPaging

Class for paging dataframes and datasets

## Example

**- Init example 1:**

Approach by specifying a limit.
```python
sp = SparkPaging(initData=df, limit=753)
```

**- Init example 2:**

Approach by specifying a number of pages (if there's a rest, the number of pages will be incremented).
```python
sp = SparkPaging(initData=df, pages=6)
```

**- Init example 3:**

Approach by specifying a limit.
```python
sp = SparkPaging()
sp.init(initData=df, limit=753)
```

**- Init example 4:**

Approach by specifying a number of pages (if there's a rest, the number of pages will be incremented).
```python
sp = SparkPaging()
sp.init(initData=df, pages=6)
```

**- Reset:**
```python
sp.reset()
```

**- Iterate example:**
```python
print("- Total number of rows = " + str(sp.initDataCount))
print("- Limit = " + str(sp.limit))
print("- Number of pages = " + str(sp.pages))
print("- Number of rows in the last page = " + str(sp.numberOfRowsInLastPage))
while (sp.page < sp.pages-1):
    df_page = sp.next()
    nbrRows = df_page.count()
    print("  Page " + str(sp.page) + '/' + str(sp.pages) + ": Number of rows = " + str(nbrRows))
```

**- Output:**
```
- Total number of rows = 4521
- Limit = 753
- Number of pages = 7
- Number of rows in the last page = 3
    Page 0/7: Number of rows = 753
    Page 1/7: Number of rows = 753
    Page 2/7: Number of rows = 753
    Page 3/7: Number of rows = 753
    Page 4/7: Number of rows = 753
    Page 5/7: Number of rows = 753
    Page 6/7: Number of rows = 3
```
