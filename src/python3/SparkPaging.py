#!/bin/python
# -*- coding: utf-8 -*-

import math

# Class to make paging with datasets and dataframes
class SparkPaging:
    '''
    - Init example 1:
        # Approach by specifying a limit.
        sp = SparkPaging(initData=df, limit=753)

    - Init example 2:
        # Approach by specifying a number of pages (if there's a rest, the number of pages will be incremented).
        sp = SparkPaging(initData=df, pages=6)

    - Init example 3:
        # Approach by specifying a limit.
        sp = SparkPaging()
        sp.init(initData=df, limit=753)

    - Init example 4:
        # Approach by specifying a number of pages (if there's a rest, the number of pages will be incremented).
        sp = SparkPaging()
        sp.init(initData=df, pages=6)

    - Reset:
        sp.reset()

    - Iterate example:
        sp.reset()
        print("- Total number of rows = " + str(sp.initDataCount))
        print("- Limit = " + str(sp.limit))
        print("- Number of pages = " + str(sp.pages))
        print("- Number of rows in the last page = " + str(sp.numberOfRowsInLastPage))
        while (sp.page < sp.pages-1):
            df_page = sp.next()
            nbrRows = df_page.count()
            print("  Page " + str(sp.page) + '/' + str(sp.pages) + ": Number of rows = " + str(nbrRows))

    - Output:
        - Total number of rows = 4521
        - Limit = 753
        - Number of pages = 7
        - Number of rows in the last page = 3
          Page 0/7: Number of rows = 753
          Page 1/7: Number of rows = 753
          Page 2/7: Number of rows = 753
          Page 3/7: Number of rows = 753
          Page 4/7: Number of rows = 753
          Page 5/7: Number of rows = 753
          Page 6/7: Number of rows = 3
    '''

    initData = None
    initDataCount = 0

    currentData = None

    pagedData = None
    page = -1

    defaultLimit = 10
    limit = 0

    defaultPages = 10
    pages = 0

    numberOfRowsInLastPage = 0

    # Constructor
    def __init__(self, initData=None, pages=0, limit=0):
        self.init(initData, pages, limit)

    # Init
    def init(self, initData, pages=0, limit=0):
        self.initData = initData
        self.initDataCount = 0
        self.reset()
        self.limit = self.defaultLimit
        self.pages = self.defaultPages
        self.numberOfRowsInLastPage = 0
        if (self.initData != None):
            self.initDataCount = self.initData.count()
            if (pages == None): pages = 0
            if (limit == None): limit = 0
            if (pages > 0): self.setPages(pages)
            else: self.setLimit(limit)

    # Set the number of pages
    def setPages(self, pages):
        if (pages <= 0): pages = self.defaultPages
        self.pages = pages
        self.limit = int(self.initDataCount/self.pages)
        self.numberOfRowsInLastPage = self.initDataCount - int(self.initDataCount/self.limit) * self.limit
        if (self.numberOfRowsInLastPage > 0): self.pages += 1

    # Set the limit value
    def setLimit(self, limit):
        if (limit <= 0): limit = self.defaultLimit
        self.limit = limit
        self.pages = math.ceil(self.initDataCount/self.limit)
        self.numberOfRowsInLastPage = self.initDataCount - int(self.initDataCount/self.limit) * self.limit

    # Reset
    def reset(self):
        self.currentData = self.initData
        self.page = -1
        self.pagedData = None

    # Paging
    def next(self):
        pagedData = None
        if (self.page < (self.pages-1)):
            self.pagedData = self.currentData.limit(self.limit)
            self.currentData = self.currentData.exceptAll(self.pagedData)
            self.page += 1
            pagedData = self.pagedData
        return pagedData
